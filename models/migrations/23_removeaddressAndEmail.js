/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
	return query.removeColumn('user', 'other_account_owner_email')
    .then(() => query.removeColumn('user', 'other_account_owner_street'))
    .then(() => query.removeColumn('user', 'other_account_owner_zip'))
    .then(() => query.removeColumn('user', 'other_account_owner_place'))
    .then(() => query.removeColumn('user', 'other_account_owner_country'))
	},
	down: async (query) => {
	    throw "Cannot downgrade";
	}
}