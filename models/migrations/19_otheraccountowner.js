/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
	return query.addColumn('user', 'other_account_owner_first_name', {type:  Sequelize.STRING,	allowNull: true})
		.then(() => query.addColumn('user', 'other_account_owner_last_name', {type:  Sequelize.STRING,	allowNull: true}) )
		.then(() => query.addColumn('user', 'other_account_owner_street', {type:  Sequelize.STRING,	allowNull: true}) )
		.then(() => query.addColumn('user', 'other_account_owner_zip', {type:  Sequelize.STRING,	allowNull: true}) )
		.then(() => query.addColumn('user', 'other_account_owner_place', {type:  Sequelize.STRING,	allowNull: true}) )
		.then(() => query.addColumn('user', 'other_account_owner_country', {type:  Sequelize.STRING,	allowNull: true}) )
	},
	down: async (query) => {
	    return query.dropColumn('user', 'other_account_owner_first_name')
	    .then(() => query.dropColumn('user', 'other_account_owner_last_name'))
	    .then(() => query.dropColumn('user', 'other_account_owner_street'))
	    .then(() => query.dropColumn('user', 'other_account_owner_zip'))
	    .then(() => query.dropColumn('user', 'other_account_owner_place'))
	    .then(() => query.dropColumn('user', 'other_account_owner_country'))
	}
}