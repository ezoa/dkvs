/* jshint esversion: 8 */
const Sequelize = require('sequelize');

module.exports = {
	up: (query) => {
		return query.addColumn('user', 'comment', {type:  Sequelize.STRING,	allowNull: true})

	},
	down: async (query) => {
	    return query.dropColumn('user', 'comment')
	}
}